var id = "";
var title = "";
var period = "7d";
var username = '';
var currenthash = '#';
var periods = ['1d', '7d', '30d', '2017'];
var intervalletje = setInterval(function() {}, 100000);
tables = {
    "opgerotte_reaguurders": {
        h2: "WEGGEJORIST EN OPGEROT",
        baby: '<img src="/img/zwitsal.png" class="inverted zwitsal">',
        username: "Nickname",
        rip: '',
        riptime: "-WEGGEJORIST EN OPGEROT-"
    },
    "reaguurder_meest_weggejorist": {
        h2: "MEEST WEGGEJORISTE REAGUURDERS",
        baby: '<img src="/img/zwitsal.png" class="inverted zwitsal">',
        username: "Nickname",
        joriscount: "Aantal Jorissen"
    },
    "reaguurder_meeste_kudos": {
        h2: "TOPREAGUURDERS",
        baby: '<img src="/img/zwitsal.png" class="inverted zwitsal">',
        username: "Nickname",
        commentkudocountsum: "Kudos"
    },
    "reaguurder_meeste_comments": {
        h2: "REAGUURDERS ZONDER LEVEN",
        baby: '<img src="/img/zwitsal.png" class="inverted zwitsal">',
        username: "Nickname",
        comments: "Aantal reaguursels"
    },
    "reaguurder_meeste_minkudos": {
        h2: "FLOPREAGUURDERS",
        baby: '<img src="/img/zwitsal.png" class="inverted zwitsal">',
        username: "Nickname",
        commentkudocountsum: "Kudos"
    },
    "reaguursel_meeste_kudos": {
        h2: "TOPREAGUURSELS",
        html: 'Comments',
        thumb: ""
    },
    "username": {
        h2: "USER",
        html: 'Comments',
        thumb: ""
    },
    "reaguursel_meeste_minkudos": {
        h2: "FLOPREAGUURSELS",
        html: 'Comments',
        thumb: ""
    },
    "tags_met_meeste_kudos": {
        h2: "TAGS MET MEESTE KUDOS",
        tag: "Tag",
        tagkudocount: "Aantal kudo's"
    },
    "topics_meest_gebruikte_tags": {
        h2: "MEEST GEBRUIKTE TAGS IN POSTS",
        tag: "Tag",
        tagcount: "Aantal posts"
    },
    "topics_meeste_comments": {
        h2: "POSTS MET MEESTE REAGUURSELS",
        title: "Post",
        commentcount: "Comments"
    },
    "topics_meeste_kudos": {
        h2: "KUDOTOPPERS",
        title: "Post",
        kudos: "Kudos"
    },
    "topics_meeste_minkudos": {
        h2: "KUDOFLOPPERS",
        title: "Post",
        kudos: "Kudos"
    },
    "topics_meeste_views": {
        h2: "POSTS MET MEESTE VIEWS",
        title: "Post",
        views: "Views"
    },
    "topics_meeste_fb_shares": {
        h2: "POSTS MET MEESTE FACEBOOK SHARES",
        title: "Post",
        fb_shares: "Facebook shares"
    },
    "topics_minste_views": {
        h2: "TOPICS MET MINSTE VIEWS",
        title: "Post",
        views: "Views"
    },
    "dumpertreeten_reaguursel_meeste_kudos": {
        h2: "DUMPERTREETEN",
        html: 'Comments',
        thumb: ""
    },
    "dimitri_reaguursel_meeste_kudos": {
        h2: 'DIMITRI',
        html: 'Comments',
        thumb: ""
    },
    "de_moeder_van_nick_reaguursel_meeste_kudos": {
        h2: 'NICK\'S MOEDER',
        html: 'Comments',
        thumb: ""
    },
    "zouhaardoen_reaguursel_meeste_kudos": {
        h2: 'ZOU HAAR DOEN',
        html: 'Comments',
        thumb: ""
    }
};

function getHashParams(h) {
    var hashParams = {};
    var e, a = /\+/g,
        // Regex for replacing addition symbol with a space
        r = /([^&;=]+)=?([^&;]*)/g,
        d = function(s) {
            return decodeURIComponent(s.replace(a, " "));
        },
        q = undefined !== h ? h.substring(1) : window.location.hash.substring(1);
    while (e = r.exec(q))
    hashParams[d(e[1])] = d(e[2]);
    return hashParams;
}

function fixDate(date, notime) {
    var year = date.substr(2, 2);
    var month = date.substr(5, 2);
    var day = date.substr(8, 2);
    var hour = date.substr(11, 2);
    var minute = date.substr(14, 2);
    fixed = day + '-' + month + '-' + year + (notime == false ? (' | ' + hour + ':' + minute) : '');
    return fixed;
}

function format(s, key, data) {
    if (key == "baby") {
        if (s == 1) {
            return ('<img src="/img/zwitsal.png" class="icon">');
        } else {
            return '';
        }
    } else if (key == "rip") {
        return ('<img src="/img/rip.png" class="icon">');
    } else if (key == "riptime") {
        return fixDate(s, true);
    } else if (key == "weggejorist") {
        if (s == 1) {
            return ('<img title="weggejorist" src="/img/deleted.png" class="deleted icon">');
        } else {
            return '';
        }
    } else if (key == "thumb") {
        return ('<a target="_blank" href="' + data['url'] + '"><img class="thumbnail" src="/img/white.png" data-src="http://media.dumpert.nl/sq_thumbs/medium/' + data['article_id'].replace('/', '_') + '.jpg"></a>');
    } else if (key == "opgerot") {
        if (s == 1) {
            return ('<img title="opgerot" src="/img/rip.png" class="opgerot icon">');
        } else {
            return '';
        }
    } else if (key == "username") {
        return '<a onclick="usernameclick(\'' + s + '\')" class="username" id="' + s + '">' + s + '</a>';
    } else if (key == "tag") {
        return '<a target="_blank" href="http://www.dumpert.nl/tag/' + s + '" class="tag" id="' + s + '">' + s + '</a>';
    } else if (key == 'title') {
        return '<a target="_blank" class="postLink" href="' + data['url'] + '"><img class="thumbnail2" src="/img/white.png" data-src="http://media.dumpert.nl/sq_thumbs/' + data['article_id'].replace('/', '_') + '.jpg">' + data['title'] + '</a>';
    } else if (key == "html") {
        if (s == '-weggejorist-') {
            s = '<p>-weggejorist-</p>'
        }
        if (s == '-weggejorist en opgerot-') {
            s = '<p>-weggejorist en opgerot-</p>'
        }
        if (data['parenthtml'] && data['parentusername'] && data['parentdatetime']) {
            parentHtml = '<div class="callout secondary parent">' + data['parenthtml'] + '<p>' + (data['parentbaby'] == '1' ? '<img src="/img/zwitsal.png" class="zwitsal small">' : '') + '<a class="strong" onclick="usernameclick(\'' + data['parentusername'] + '\')">' + data['parentusername'] + '</a> <br /> ' + fixDate(data['parentdatetime'], false) + ' <strong class="label ' + (data['parentcommentkudocount'] < 0 ? 'alert' : 'success') + '">' + (data['parentcommentkudocount']) + '</strong> ' + (data['weggejorist'] == '1' ? '<img title="weggejorist" src="/img/deleted.png" class="deleted icon">' : '') + ' ' + (data['opgerot'] == '1' ? '<img title="opgerot" src="/img/rip.png" class="opgerot icon">' : '') + '</p></div>';
        } else {
            parentHtml = '';
        }
        return '<div id="' + data['commentid'] + '" class="html"><a target="_blank" class="commentTitle" href="' + data['url'] + '">' + data['title'] + '</a>' + parentHtml + '<div class="callout ' + (data['parenthtml'] ? 'indent' : '') + '">' + s + '<p class="datetime">' + (data['baby'] == '1' ? '<img src="/img/zwitsal.png" class="zwitsal small">' : '') + '<a class="strong" onclick="usernameclick(\'' + data['username'] + '\')">' + data['username'] + '</a> <br /> ' + fixDate(data['datetime'], false) + ' <strong class="label ' + (data['commentkudocount'] < 0 ? 'alert' : 'success') + '">' + (data['commentkudocount']) + '</strong> ' + (data['weggejorist'] == '1' ? '<img title="weggejorist" src="/img/deleted.png" class="deleted icon">' : '') + ' ' + (data['opgerot'] == '1' ? '<img title="opgerot" src="/img/rip.png" class="opgerot icon">' : '') + '</p></div></div>';
    } else if (false === isNaN(s)) {
        s += '';
        x = s.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        res = x1 + x2;
        return '<strong class="label ' + (res < 0 ? 'alert' : 'success') + '">' + (res) + '</strong>';
    } else {
        return s;
    }
}

function formatN(s) {
    s += '';
    x = s.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    res = x1 + x2;
    return res;
}

function getNow() {
    var now = new Date();
    var mins = now.getMinutes();
    var quarterHours = Math.round(mins / 15);
    if (quarterHours == 4) {
        now.setHours(now.getHours() + 1);
    }
    var rounded = (quarterHours * 15) % 60;
    now.setMinutes(rounded);
    return escape(now);
}

function getContent(id, username, period, comment) {
    data = null;
    realdata = null;
    $('.period').removeClass('success');
    $('#' + period).addClass('success');
    $('#modalRank').hide();
    realid = id;
    if (id == 'gezeur') {
        $('#modalContent').hide();
        $('#modalPage').html($('#gezeurDiv').html())
        $('#modalPage').show();
        currenthash = '#i=gezeur';
        window.location.hash = currenthash;
        $('title').html('Gezeur | DumpertStats');
    } else {
        if (id.search('username__') === 0) {
            username = id.replace('username__', '');
            id = 'username';
            $('.periodSelector').hide();
            url = "/data/reaguurders/" + md5(username) + '_comments.json?' + getNow();
        } else {
            url = "/data/lijstjes/" + id + '_' + period + '.json?' + getNow();
            $('.periodSelector').show();
        }
        $('.td_count,.count').show();
        $('#modalContent').show();
        $('#modalPage').hide();
        $('#modalTable thead').html('<tr>');
        $('#modalTable thead').append('<th class="count"><img src="/img/spinner.gif" class="spinner"></th>');
        $('#modalTable tbody').html('');
        for (var item in tables[id]) {
            if (item != "h2") {
                $('#modalTable thead').append('<th class="td_' + item + '">' + tables[id][item] + '</th>');
            }
        }
        $('#modalTable thead').append('</tr>');
        title = tables[id]['h2'];
        if (title == 'USER') {
            title = username;
        }
        $('#modalHeader h2').html(title);
        setTimeout(function() {
            $.ajax({
                url: url,
                context: document.body
            }).done(function(data) {
                clearInterval(intervalletje);
                n = 0;
                realdata = data;
                if (realid.search('username__') === 0) {
                    data = data['comments'];
                }
                $(data).each(function(i) {
                    n++;
                    if (undefined === (data[i][item])) {
                        data[i][item] = '';
                    }
                    $('#modalTable tbody').append('<tr>');
                    $('#modalTable tbody').append('<td class="td_count">' + n + '.</td>');
                    for (var item in tables[id]) {
                        if (item != "h2") {
                            $('#modalTable tbody').append('<td class="td_' + item + '">' + format(data[i][item], item, data[i]) + '</td>');
                        }
                    };
                    $('#modalTable tbody').append('</tr>');
                });
                $('.spinner').hide();
                if (realid.search('username__') === 0) {
                    $('.td_count,.count').hide();
                }
                currenthash = '#i=' + escape(realid != null ? realid : '') + (period != null ? '&p=' + period : '') + (comment != null ? '&c=' + comment : '');
                window.location.hash = currenthash;
                $('title').html(title+' | DumpertStats');
                if (realid.search('username__') === 0) {
                    $('#rank_2017').html((realdata['commentcount']['2017'] == 0 ? '-' : '#' + formatN(realdata['rank']['kudos']['2017'])));
                    $('#rank_30d').html((realdata['commentcount']['30d'] == 0 ? '-' : '#' + formatN(realdata['rank']['kudos']['30d'])));
                    $('#rank_7d').html((realdata['commentcount']['7d'] == 0 ? '-' : '#' + formatN(realdata['rank']['kudos']['7d'])));
                    $('#rank_1d').html((realdata['commentcount']['1d'] == 0 ? '-' : '#' + formatN(realdata['rank']['kudos']['1d'])));
                    $('#kudos_2017').html(formatN(realdata['kudos']['2017']));
                    $('#kudos_30d').html(formatN(realdata['kudos']['30d']));
                    $('#kudos_7d').html(formatN(realdata['kudos']['7d']));
                    $('#kudos_1d').html(formatN(realdata['kudos']['1d']));
                    $('#comments_2017').html(formatN(realdata['commentcount']['2017']));
                    $('#comments_30d').html(formatN(realdata['commentcount']['30d']));
                    $('#comments_7d').html(formatN(realdata['commentcount']['7d']));
                    $('#comments_1d').html(formatN(realdata['commentcount']['1d']));
                    $('#weggejorist_2017').html(formatN(realdata['weggejorist']['2017']));
                    $('#weggejorist_30d').html(formatN(realdata['weggejorist']['30d']));
                    $('#weggejorist_7d').html(formatN(realdata['weggejorist']['7d']));
                    $('#weggejorist_1d').html(formatN(realdata['weggejorist']['1d']));
                    $('#modalRank').show();
                }
                $("img").unveil(500);
                intervalletje = setInterval(function() {
                    $(window).trigger("lookup");
                }, 500);
                if (comment) {
                    $('.reveal-overlay').animate({
                        scrollTop: $('#' + comment).offset().top + 'px'
                    }, 'fast');
                }
            });
        }, 300);
    }
}

function usernameclick(username) {
    log('username_' + username, period);
    id = 'username__' + username;
    getContent(id, username, period, null);
}

function log(id, period) {
    ga('set', 'page', currenthash);
    ga('send', 'pageview');
}

function load(h) {
    if (undefined !== h) {
        params = getHashParams(h);
    } else {
        params = getHashParams();
    }
    if (params['p']) {
        $(periods).each(function(key, val) {
            if (params['p'] == val) {
                period = val;
            }
        });
    }
    if (params['i']) {
        id = params['i'];
    }
    if (params['r'] && params['c']) {
        username = params['r'];
        log('username_' + username, period);
        id = 'username__' + username;
        $('#theModal').foundation('open').fadeIn();
        getContent(id, username, period, params['c']);
    } else if (id) {
        $('#theModal').foundation('open').fadeIn();
        getContent(id, username, period, null);
    } else if (window.location.hash == '#') {
        $('#theModal').foundation('close').fadeOut();
    }
}
$(window).on('closed.zf.reveal', function() {
    $('#modalTable tbody').html('');
    currenthash = '#';
    window.location.hash = currenthash;
    $('title').html('DumpertStats');
});
window.onhashchange = function() {
    if (window.location.hash != currenthash && window.location.hash != '#' && window.location.hash != '') {
        $('#modalTable tbody').html('');
        load(window.location.hash);
    } else if (window.location.hash == '') {
        $('#theModal').foundation('close').fadeOut();
    }
};
$('document').ready(function() {
    isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    if (isChrome === false) {
        $('#chrome').hide();
    }
    $('#reaguurders').focus(function() {
        if (window.innerWidth < 600) {
            $('body,html').animate({
                scrollTop: ($('#reaguurders').offset().top - 30) + 'px'
            }, 'fast');
        }
    });
    $('input').keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    $("#update").load("/data/lijstjes/last_updated.html");
    $('body').show();
    $("#reaguurders").autocomplete({
        source: alleReaguurders,
        minLength: 3,
        select: function(event, ui) {
            if (ui.item) {
                username = ui.item.value;
                log('username_' + username, period);
                id = 'username__' + username;
                $('#theModal').foundation('open').fadeIn();
                getContent(id, username, period, null);
                setTimeout(function() {
                    $('input').val('');
                    $('input').blur();
                }, 1000);
            }
            //            $('#zoekReaguurder').submit();
        }
    });
    $('.trigger').on('click', function(e) {
        e.preventDefault();
        id = $(this).attr('id');
        $('#theModal').foundation('open').fadeIn();
        getContent(id, username, period, null);
    });
    $('.period').on('click', function(e) {
        e.preventDefault();
        period = $(this).attr('id');
        getContent(id, username, period, null);
    });
    $('a').on('click', function() {
        if ($(this).attr('id')) {
            log(id, period);
        }
    });
    $('#' + period).addClass('success');
    load();
});
$(window).bind("load resize scroll", function() {
    var footer = $("#footer");
    var pos = footer.position();
    var height = $(window).height();
    height = height - pos.top;
    height = height - footer.height();
    if (height > 0) {
        footer.css({
            'margin-top': height + 'px'
        });
    }
});